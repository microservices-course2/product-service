#!/usr/bin/env node

/**
 * Module dependencies.
 */

const app = require("../app");
const debug = require("debug")("auth-service:server");
const http = require("http");
const connectRabbitMQ = require("../config/rabbitmq");

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

async function onListening() {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  debug("Listening on " + bind);

  // connect rabbitmq server
  const channel = await connectRabbitMQ();
  // รับข้อมูลจาก auth-service
  await channel.consume("q.akenarin.product.service", (msg) => {
    if (msg !== null) {
      console.log(msg.content.toString());
      // await User.create({ fullname: msg.content.fullname })
      // app.set("user", msg.content);
      console.log(msg.properties.type);
      //if (msg.properties.type === 'UserCreated') {
      //await User.create({ fullname: msg.content.fullname })
      //} else if (msg.properties.type === "UserUpdate") {
      //await User.update({ fullname: msg.content.fullname })
      //}

      channel.ack(msg);
    } else {
      console.log("ไม่พบข้อมูลจาก Server");
    }
  });

  // รับข้อมูลจาก auth-service แบบ direct
  await channel.consume("q.akenarin.direct.product.service", (msg) => {
    if (msg !== null) {
      console.log(msg.content.toString());
      console.log(msg.properties.type);
      channel.ack(msg);
    } else {
      console.log("ไม่พบข้อมูลจาก Server");
    }
  });

}
