const express = require('express');
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();
const mongoose = require("mongoose");

const productRouter = require("./routes/product");

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// connect to mongodb server
mongoose.connect(process.env.MONGODB_URI); // from .env

// app.use('/', indexRouter);
// localhost:5000/api/v1/product
app.use('/api/v1/product', productRouter);

module.exports = app;
