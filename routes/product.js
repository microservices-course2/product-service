const express = require('express');
const router = express.Router();
const Product = require("../models/product");

const passportJWT = require('../middlewares/passport-jwt');
const checkAdmin = require('../middlewares/check-admin');

// localhost:5000/api/v1/product/
// สำหรับ Admin เท่านั้น
router.post("/", [passportJWT.checkAuth, checkAdmin.checkAdminRole] , async function (req, res, next) {
  
  const newProduct = await Product.create(req.body);

  return res.status(201).json({
    message: "เพิ่มสินค้าสำเร็จ",
    data: newProduct
  });

});

module.exports = router;
