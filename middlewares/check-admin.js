module.exports.checkAdminRole = (req, res, next) => {

    try {
     const { role } = req.user; // req.user.role

     if (role === "admin") {
       next();
     } else {
       return res.status(403).json({ message: "สำหรับ Admin เท่านั้น" });
     }  
      
    } catch (error) {
        next(error)
    }
    
}